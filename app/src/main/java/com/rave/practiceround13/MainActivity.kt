package com.rave.practiceround13

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Entry point for application.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
